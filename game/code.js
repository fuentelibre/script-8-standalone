// title: Voyager II
// license: AGPL-3
// author: icarito

initialState = {
  color: 0,
  speed: 7,
  timer: 0,
  fire: false,
  frame: 0,
  debug: false,
  fps: 0,
  stars: [],
  asteroids: [],
  pos: {x: 64,
        y: 64}
}

update = (state, input, elapsed)=> {
  state.color = 5
  state.timer = state.timer + elapsed
  state.frame += 1
  state.fps = Math.round(1000/elapsed)
  if (state.stars.length < 12) {
    if (random(0, 20) > 14) {
      state.stars.push([128, random(11,127), random(3,7), random(1, 3)])
                      // x, y, color, speed
    }
  }
  if (state.asteroids.length < 3 + state.frame / 1000) {
    if (random(0, 100) > 100 - state.speed) {
      state.asteroids.push([128, random(11,119), random(0, 2), random(), 
                            random(0, 3), random(0, 5)])
                      // x, y, animFrame, flipped, color
    }
  }
  state.stars.forEach( (ast)=> {
    ast[0] -= state.speed * ast[3]
    if (ast[0] < - 8) {
      state.stars.splice(state.stars.indexOf(ast), 1)
    }
  })
  state.asteroids.forEach( (ast)=> {
    ast[0] -= state.speed / (2 - ast[5]/5) + ast[5]/20
    if (ast[0] < - 8) {
      state.asteroids.splice(state.asteroids.indexOf(ast), 1)
    }
  })
  if (input.up) {
    state.pos.y -= 1
  }
  else if (input.down) {
    state.pos.y += 1
  }
  
  if (input.left) {
    if (!input.select) {
      state.pos.x -= 1
    }
    state.speed -= 0.05
  }
  else if (input.right) {
    if (!input.select) {
      state.pos.x += 1
    }
    state.speed += 0.05
  }
  if (input.a) {
    state.fire = true
  }
  else {
    state.fire = false
  }
  state.speed = clamp(state.speed, 0, 8)
  state.pos.x = clamp(state.pos.x, 1, 111)
  state.pos.y = clamp(state.pos.y, 7, 112)
}

function drawShip(pos) {
  let x = pos.x
  let y = pos.y
  sprite(x, y, 0)
  sprite(x + 8, y, 1)
  sprite(x, y + 8, 16)
  sprite(x + 8, y + 8, 17)
}

draw = (state)=> {
  clear()
  line (0, 8, 128, 8, state.color)
    
  state.stars.forEach( (ast)=> {    
        line(ast[0], ast[1], ast[0] + ast[3] * 2 * state.speed, ast[1], ast[2])
  })

  state.asteroids.forEach( (ast)=> {
    let animFrame = Math.round(state.frame / 20) % 3
    sprite(ast[0], ast[1], (ast[2] + animFrame) % 3  + 2, ast[4], ast[3])  
  })
  
  drawShip(state.pos)
  if (state.fire===true) {
    line(state.pos.x + 16, state.pos.y + 8, 128, state.pos.y + random(4,12), 3)
  }
  
  if (state.debug) {
    print (4, 2, 'frame: ' + state.frame)
    print (100, 2, state.fps + ' fps')
  } 
  else {
    print (4, 2, 'Voyager II')
    print (95, 2, 'warp ' + Math.round(state.speed*10)/10)
  }
  
  rectStroke ( 0, 0, 128, 128, state.color)
}
