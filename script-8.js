const range = require('lodash/range')
const clamp = require('lodash/clamp')
const random = require('lodash/random')
import canvasAPI from './frameBufferCanvasAPI/index.js'
import getUserInput from './getUserInput.js'

const CANVAS_SIZE = 128

const canvas = document.getElementById('game')

var keys = new Set()
function keydownHandler(e) {
  keys.add(e.key)
}
function keyupHandler(e) {
  keys.delete(e.key)
}
document.addEventListener('keydown', keydownHandler)
document.addEventListener('keyup', keyupHandler)

/*var update = function update(state, input, elapsed) {
    state.t += 1
    if (input.up) {
        state.y -= 1
    }
    if (input.down) {
        state.y += 1
    }
    if (input.left) {
        state.x -= 1
    }
    if (input.right) {
        state.x += 1
    }
}

var initialState = {}

var draw = function draw(state) {
    clear(4)
    print(10,10,'Works! ' + state.t)
    sprite(state.x,state.y,0)
}
*/

function evalInContext(js, context) {
    return function() { return eval(js); }.call(context)
}

const { search } = window.location
const params = new window.URLSearchParams(search)
const urlId = params.get('id')

function _get(yourUrl){
    let Httpreq = new XMLHttpRequest()
    Httpreq.open("GET", yourUrl, false)
    Httpreq.send(null)
    return Httpreq.responseText
}

if (urlId && urlId.length==32) {
    let gist = JSON.parse(_get("https://api.github.com/gists/" + urlId))
    var _sprites = JSON.parse(gist.files['sprites.json'].content)
    var _map = JSON.parse(gist.files['map.json'].content)
    var _code = gist.files['code.js'].content
}
else
{
    var _sprites = JSON.parse(_get("./game/sprites.json"))
    var _map = JSON.parse(_get("./game/map.json"))
    var _code = _get("./game/code.js")
}

var _pixelData = new ImageData(CANVAS_SIZE, CANVAS_SIZE)
var _pixelBuffer = new ArrayBuffer(4 * CANVAS_SIZE * CANVAS_SIZE)
var _pixelBytes = new Uint8ClampedArray(_pixelBuffer)
var _pixelIntegers = new Uint32Array(_pixelBuffer)
_pixelIntegers.fill(0)

const { polyStroke, getTile, setTile, line, print, rectStroke, camera,
        rectFill, resetMap, map, sprite, circStroke, circFill,
        clear, getPixel, setPixel } = canvasAPI( 
          { pixels: _pixelIntegers,
            ctx: canvas.getContext('2d'),
            width: CANVAS_SIZE,
            height: CANVAS_SIZE,
            sprites: _sprites,
            map: _map
          })
var initialState = {}
var update = function() {}
var draw = function() {}

evalInContext(_code,  { random, range, clamp, polyStroke, getTile, setTile, line, print, rectStroke, camera,
    rectFill, resetMap, map, sprite, circStroke, circFill, clear, getPixel, setPixel })

var time = (new Date()).getTime()
var elapsed = 0
var state = initialState
var input

function loop() {
    time = (new Date()).getTime()
    input = getUserInput(keys)
    evalInContext("update(state, input, elapsed)",  { random, polyStroke, getTile, setTile, line, print, rectStroke, 
        camera, rectFill, resetMap, map, sprite, circStroke, circFill, clear, getPixel, setPixel, range })
    evalInContext("draw(state)",  { random, polyStroke, getTile, setTile, line, print, rectStroke, camera,
        rectFill, resetMap, map, sprite, circStroke, circFill, clear, getPixel, setPixel, range })

    _pixelData.data.set(_pixelBytes)
    const ctx = canvas.getContext('2d')
    ctx.putImageData(_pixelData, 0, 0)

    elapsed = Number((new Date()).getTime() - time);
    window.requestAnimationFrame( loop )
    }
window.requestAnimationFrame( loop )

