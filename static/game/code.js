// title: Fire effect
// adapted from http://fabiensanglard.net/doom_fire_psx/

const width = 128
const height = 64
const offset = 64

var fb = []

range(0, width).forEach((x)=>
{
  fb[x] = []
  range(0, height-1).forEach((y)=>
  {
    fb[x][y]=7
  })
  fb[x][height-1] = 1
})

initialState = {
  firePixels: fb,
  frame: 0,
  wind: 0,
  intensity: 2,
  elapsed: 0
}

update = (state, input, elapsed) => {
  state.elapsed = elapsed
  if (input.right) {
    state.wind = clamp(state.wind + 0.3, -3, 3)
  } else if (input.left) {
    state.wind = clamp(state.wind - 0.3, -3, 3)
  } else {
    state.wind = state.wind * 0.8
  }
  if (input.up) {
    state.intensity = clamp(state.intensity + 0.5, 2, 20)
  } else if (input.down) {
    state.intensity = 0
  } else {
    state.intensity = 2
  }
  for (let y of range(height-2, 0, -1)) {
    for (let x of range(0, width)) {
      var rand = Math.round(Math.random());
      var rand2 = Math.round(Math.random());
      var dst = Math.round(clamp(x - rand + rand2 + state.wind, 0, width-1))
      state.firePixels[dst][y-rand * state.intensity] = clamp(state.firePixels[x][y+1] + rand, 0, 7)
    }
  }
}

draw = (state)=> {
  clear()
  print(32, 12, '' + Math.round(1000.0 / state.elapsed));
  for (let x of range(0, width)) {
    for (let y of range(0, height)) {
      line(x,y + offset,x,y + offset,state.firePixels[x][y])
    }
  }
}
